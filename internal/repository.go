package internal

import "gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"

// BeneficiaryRepository interface for benificiary repo
type BeneficiaryRepository interface {
	Save(beneficiary *models.Beneficiary) (*models.Beneficiary, error)
	GetByID(id string) (*models.Beneficiary, error)
	Get(search map[string]interface{}) ([]*models.Beneficiary, error)
	Count(search map[string]interface{}) (int64, error)
	Update(id string, beneficiary *models.Beneficiary) (*models.Beneficiary, error)
}

// CategoryRepository interface for category repo
type CategoryRepository interface {
	GetByID(id string) (*models.Category, error)
	Get() ([]*models.Category, error)
}

// ContributorRepository interface for contributor repo
type ContributorRepository interface {
	Save(contributor *models.Contributor) (*models.Contributor, error)
	GetByID(id string) (*models.Contributor, error)
	Get(search map[string]interface{}) ([]*models.Contributor, error)
	Count(search map[string]interface{}) (int64, error)
}

type UserRepository interface {
	Save(user *models.User) (*models.User, error)
	IsUserRegistered(userName string, phoneNumber string) (bool, error)
	GetUserByUsername(userName string) (*models.User, error)
}
