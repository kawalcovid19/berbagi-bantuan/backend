package beneficiary

import (
	"time"

	"github.com/google/uuid"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type BeneficiaryService interface {
	SaveBeneficiary(payload BeneficiaryPayload) (*models.Beneficiary, error)
	DetailBeneficiary(id string) (*models.Beneficiary, error)
	GetBeneficiary(search map[string]interface{}) ([]*models.Beneficiary, int64, error)
	UpdateBeneficiary(id, username string) (*models.Beneficiary, error)
}

type beneficiaryService struct {
	beneficiaryRepo internal.BeneficiaryRepository
}

func NewBeneficiaryService(beneficiary internal.BeneficiaryRepository) (BeneficiaryService, error) {
	return &beneficiaryService{
		beneficiaryRepo: beneficiary,
	}, nil
}

func (r *beneficiaryService) SaveBeneficiary(payload BeneficiaryPayload) (*models.Beneficiary, error) {
	beneficiary := new(models.Beneficiary)
	beneficiary.ID = uuid.New().String()

	beneficiary.Name = payload.Name
	beneficiary.Category = payload.Category
	beneficiary.Amount = payload.Amount
	beneficiary.ProviderName = payload.ProviderName
	beneficiary.ReceiverName = payload.ReceiverName
	beneficiary.Note = payload.Note
	beneficiary.Phone = payload.Phone
	beneficiary.IsWhatsAppAvailable = payload.IsWhatsAppAvailable
	beneficiary.CreatedAt = time.Now().UTC()

	beneficiary.ItemDetails = make([]models.ItemDetail, 0)
	for _, val := range payload.ItemDetails {
		item := models.ItemDetail{
			Name:     val.Name,
			Quantity: val.Quantity,
		}

		beneficiary.ItemDetails = append(beneficiary.ItemDetails, item)
	}

	beneficiary.IsActive = true
	beneficiary.Location = models.Location{
		Name:        payload.Location.Name,
		Country:     payload.Location.Country,
		City:        payload.Location.City,
		Address:     payload.Location.Address,
		Longitude:   payload.Location.Longitude,
		Latitude:    payload.Location.Latitude,
		GeoLocation: models.CreateGeoJsonPoint(payload.Location.Longitude, payload.Location.Latitude),
	}

	beneficiary, err := r.beneficiaryRepo.Save(beneficiary)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}

func (r *beneficiaryService) DetailBeneficiary(id string) (*models.Beneficiary, error) {

	beneficiary, err := r.beneficiaryRepo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}

func (r *beneficiaryService) GetBeneficiary(search map[string]interface{}) ([]*models.Beneficiary, int64, error) {
	count, err := r.beneficiaryRepo.Count(search)
	if err != nil {
		return nil, 0, err
	}

	res, err := r.beneficiaryRepo.Get(search)
	if err != nil {
		return nil, 0, err
	}

	latitude, okLat := search["latitude"].(float64)
	longitude, okLng := search["longitude"].(float64)
	if !okLat || !okLng {
		return res, count, nil
	}

	sourceLocation := kit.Point{
		Latitude:  latitude,
		Longitude: longitude,
	}

	for i, val := range res {
		destLocation := kit.Point{
			Latitude:  val.Location.GeoLocation.Latitude(),
			Longitude: val.Location.GeoLocation.Longitude(),
		}

		val.Distance = new(models.Distance)
		val.Distance.Length, val.Distance.Unit = kit.Distance(sourceLocation, destLocation)
		res[i] = val
	}

	return res, count, nil
}

func (r *beneficiaryService) UpdateBeneficiary(id, username string) (*models.Beneficiary, error) {
	beneficiary := new(models.Beneficiary)
	beneficiary.ID = id
	beneficiary.IsVerified = true
	beneficiary.UpdatedBy = username
	beneficiary.UpdatedAt = time.Now().UTC()
	beneficiary, err := r.beneficiaryRepo.Update(id, beneficiary)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}
