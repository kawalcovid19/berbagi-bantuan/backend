package beneficiary

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
)

var (
	ioutilReadAll = ioutil.ReadAll
)

const (
	userNameHeader = "username"
	rolesHeader    = "roles"
)

type BeneficiaryHandler struct {
	regServ BeneficiaryService
}

func NewBeneficiaryHandler(rserv BeneficiaryService) (*BeneficiaryHandler, error) {
	return &BeneficiaryHandler{
		regServ: rserv,
	}, nil
}

func (rh *BeneficiaryHandler) Register(router *httprouter.Router) {
	router.HandlerFunc("POST", "/beneficiary", rh.CreateBeneficiary)
	router.HandlerFunc("GET", "/beneficiary/:id", rh.DetailBeneficiary)
	router.HandlerFunc("GET", "/beneficiary", rh.ListBeneficiary)
	router.HandlerFunc("PUT", "/beneficiary/:id", rh.UpdateBeneficiary)
}

func (rh *BeneficiaryHandler) CreateBeneficiary(w http.ResponseWriter, r *http.Request) {
	body, err := ioutilReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload BeneficiaryPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	if err = payload.Validate(); err != nil {
		log.Printf("Error while validate payload : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	beneficiary, err := rh.regServ.SaveBeneficiary(payload)
	if err != nil {
		log.Printf("Error while create beneficiary : %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(beneficiary)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(res)
}

func (rh *BeneficiaryHandler) DetailBeneficiary(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	beneficiary, err := rh.regServ.DetailBeneficiary(params.ByName("id"))
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		if err.Error() == "not found" {
			msg = "not found"
			status = http.StatusNotFound
		} else {
			log.Printf("Error while get beneficiary : %v", err)
		}

		http.Error(w, msg, status)
		return
	}

	res, _ := json.Marshal(beneficiary)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}

func (rh *BeneficiaryHandler) ListBeneficiary(w http.ResponseWriter, r *http.Request) {
	search := internal.ParseSearch(r.URL.Query())

	beneficiaries, total, err := rh.regServ.GetBeneficiary(search)
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		log.Printf("Error while get beneficiary : %v", err)

		http.Error(w, msg, status)
		return
	}

	data := struct {
		Data  interface{} `json:"data"`
		Limit int64       `json:"limit"`
		Page  int64       `json:"page"`
		Total int64       `json:"total"`
	}{
		Data:  beneficiaries,
		Limit: search["limit"].(int64),
		Page:  search["page"].(int64),
		Total: total,
	}

	res, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}

func (rh *BeneficiaryHandler) UpdateBeneficiary(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	beneficiary, err := rh.regServ.UpdateBeneficiary(params.ByName("id"), r.Header.Get(userNameHeader))
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		if err.Error() == "not found" {
			msg = "not found"
			status = http.StatusNotFound
		} else {
			log.Printf("Error while update beneficiary : %v", err)
		}

		http.Error(w, msg, status)
		return
	}

	res, _ := json.Marshal(beneficiary)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}
