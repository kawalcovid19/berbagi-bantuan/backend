package contributor

import (
	"time"

	"github.com/google/uuid"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type ContributorService interface {
	SaveContributor(payload ContributorPayload) (*models.Contributor, error)
	DetailContributor(id string) (*models.Contributor, error)
	GetContributor(search map[string]interface{}) ([]*models.Contributor, int64, error)
}

type contributorService struct {
	contributorRepo internal.ContributorRepository
}

func NewContributorService(contributor internal.ContributorRepository) (ContributorService, error) {
	return &contributorService{
		contributorRepo: contributor,
	}, nil
}

func (c *contributorService) SaveContributor(payload ContributorPayload) (*models.Contributor, error) {
	contributor := new(models.Contributor)
	contributor.ID = uuid.New().String()

	contributor.Name = payload.Name
	contributor.Category = payload.Category
	contributor.Amount = payload.Amount
	contributor.ProviderName = payload.ProviderName
	contributor.Phone = payload.Phone
	contributor.Note = payload.Note
	contributor.IsPhoneNumberHidden = payload.IsPhoneNumberHidden
	contributor.IsWhatsAppAvailable = payload.IsWhatsAppAvailable
	contributor.CreatedAt = time.Now().UTC()

	contributor.IsActive = true
	contributor.ItemDetails = make([]models.ItemDetail, 0)
	for _, val := range payload.ItemDetails {
		item := models.ItemDetail{
			Name:     val.Name,
			Quantity: val.Quantity,
		}

		contributor.ItemDetails = append(contributor.ItemDetails, item)
	}

	contributor.Location = models.Location{
		Name:        payload.Location.Name,
		Country:     payload.Location.Country,
		City:        payload.Location.City,
		Address:     payload.Location.Address,
		Longitude:   payload.Location.Longitude,
		Latitude:    payload.Location.Latitude,
		GeoLocation: models.CreateGeoJsonPoint(payload.Location.Longitude, payload.Location.Latitude),
	}

	contributor, err := c.contributorRepo.Save(contributor)
	if err != nil {
		return nil, err
	}

	return contributor, nil
}

func (c *contributorService) DetailContributor(id string) (*models.Contributor, error) {

	contributor, err := c.contributorRepo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return contributor, nil
}

func (c *contributorService) GetContributor(search map[string]interface{}) ([]*models.Contributor, int64, error) {
	count, err := c.contributorRepo.Count(search)
	if err != nil {
		return nil, 0, err
	}

	res, err := c.contributorRepo.Get(search)
	if err != nil {
		return nil, 0, err
	}

	latitude, okLat := search["latitude"].(float64)
	longitude, okLng := search["longitude"].(float64)
	if !okLat || !okLng {
		return res, count, nil
	}

	sourceLocation := kit.Point{
		Latitude:  latitude,
		Longitude: longitude,
	}

	for i, val := range res {
		destLocation := kit.Point{
			Latitude:  val.Location.GeoLocation.Latitude(),
			Longitude: val.Location.GeoLocation.Longitude(),
		}

		val.Distance = new(models.Distance)
		val.Distance.Length, val.Distance.Unit = kit.Distance(sourceLocation, destLocation)
		res[i] = val
	}

	return res, count, nil
}
