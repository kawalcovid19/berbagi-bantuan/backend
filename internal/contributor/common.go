package contributor

import (
	"fmt"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type ContributorPayload struct {
	ID                  string                   `json:"id"`
	Name                string                   `json:"name"`
	Category            string                   `json:"category"`
	Amount              int                      `json:"amount"`
	ProviderName        string                   `json:"provider_name"`
	Phone               string                   `json:"phone"`
	Note                string                   `json:"note"`
	IsPhoneNumberHidden bool                     `json:"is_phone_number_hidden"`
	IsWhatsAppAvailable bool                     `json:"is_whatsapp_available"`
	ItemDetails         []ItemDetailPayload      `json:"item_details"`
	Location            internal.LocationPayload `json:"location"`
}

type ItemDetailPayload struct {
	Name     string `json:"name"`
	Quantity int    `json:"quantity"`
}

func (c *ContributorPayload) Validate() error {
	if c.Name == "" {
		return fmt.Errorf("name is missing")
	}
	if c.Category == "" {
		return fmt.Errorf("category is missing")
	}
	if c.Amount == 0 {
		return fmt.Errorf("amount is missing")
	}
	if c.ProviderName == "" {
		return fmt.Errorf("provider name is missing")
	}
	if c.Phone == "" {
		return fmt.Errorf("phone is missing")
	}

	return nil
}

func CheckHiddenPhoneNumber(contributor *models.Contributor) *models.Contributor {
	if contributor.IsPhoneNumberHidden {
		contributor.Phone = contributor.Phone[:len(contributor.Phone)-5] + "xxxxx"
	}
	return contributor
}
