package contributor

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
)

var (
	ioutilReadAll = ioutil.ReadAll
)

type ContributorHandler struct {
	contributorServ ContributorService
}

func NewContributorHandler(cserv ContributorService) (*ContributorHandler, error) {
	return &ContributorHandler{
		contributorServ: cserv,
	}, nil
}

func (ch *ContributorHandler) Register(router *httprouter.Router) {
	router.HandlerFunc("POST", "/contributor", ch.CreateContributor)
	router.HandlerFunc("GET", "/contributor/:id", ch.DetailContributor)
	router.HandlerFunc("GET", "/contributor", ch.ListContributor)
}

func (ch *ContributorHandler) CreateContributor(w http.ResponseWriter, r *http.Request) {
	body, err := ioutilReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload ContributorPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	if err = payload.Validate(); err != nil {
		log.Printf("Error while validate payload : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	contributor, err := ch.contributorServ.SaveContributor(payload)
	if err != nil {
		log.Printf("Error while create contributor : %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(contributor)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(res)
}

func (ch *ContributorHandler) DetailContributor(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	contributor, err := ch.contributorServ.DetailContributor(params.ByName("id"))
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		if err.Error() == "not found" {
			msg = "not found"
			status = http.StatusNotFound
		} else {
			log.Printf("Error while get contributor : %v", err)
		}

		http.Error(w, msg, status)
		return
	}

	res, _ := json.Marshal(CheckHiddenPhoneNumber(contributor))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}

func (ch *ContributorHandler) ListContributor(w http.ResponseWriter, r *http.Request) {
	search := internal.ParseSearch(r.URL.Query())

	contributors, total, err := ch.contributorServ.GetContributor(search)
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		log.Printf("Error while get contributor : %v", err)

		http.Error(w, msg, status)
		return
	}

	for _, contributor := range contributors {
		contributor = CheckHiddenPhoneNumber(contributor)
	}

	data := struct {
		Data  interface{} `json:"data"`
		Limit int64       `json:"limit"`
		Page  int64       `json:"page"`
		Total int64       `json:"total"`
	}{
		Data:  contributors,
		Limit: search["limit"].(int64),
		Page:  search["page"].(int64),
		Total: total,
	}

	res, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}
