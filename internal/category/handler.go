package category

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var (
	ioutilReadAll = ioutil.ReadAll
)

type CategoryHandler struct {
	regServ CategoryService
}

func NewCategoryHandler(rserv CategoryService) (*CategoryHandler, error) {
	return &CategoryHandler{
		regServ: rserv,
	}, nil
}

func (rh *CategoryHandler) Register(router *httprouter.Router) {
	router.HandlerFunc("GET", "/category", rh.ListCategory)
}

func (rh *CategoryHandler) ListCategory(w http.ResponseWriter, r *http.Request) {

	categories, total, err := rh.regServ.GetCategory()
	if err != nil {
		msg := "internal server error"
		status := http.StatusInternalServerError

		if err.Error() == "not found" {
			msg = "not found"
			status = http.StatusNotFound
		} else {
			log.Printf("Error while get category : %v", err)
		}

		http.Error(w, msg, status)
		return
	}

	data := struct {
		Data  interface{} `json:"name"`
		Total int         `json:"total"`
	}{
		Data:  categories,
		Total: total,
	}

	res, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}
