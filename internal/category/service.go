package category

import (
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type CategoryService interface {
	GetCategory() ([]*models.Category, int, error)
}

type categoryService struct {
	categoryRepo internal.CategoryRepository
}

func NewCategoryService(category internal.CategoryRepository) (CategoryService, error) {
	return &categoryService{
		categoryRepo: category,
	}, nil
}

func (r *categoryService) GetCategory() ([]*models.Category, int, error) {

	res, err := r.categoryRepo.Get()
	if err != nil {
		return nil, 0, err
	}

	return res, len(res), nil
}
