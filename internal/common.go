package internal

import (
	"net/url"
	"strconv"
)

const defaultLimit = int64(10)

type LocationPayload struct {
	Name      string  `json:"name"`
	Country   string  `json:"country"`
	City      string  `json:"city"`
	Address   string  `json:"address"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

var (
	valType = map[string]string{
		"is_active":   "bool",
		"limit":       "int64",
		"page":        "int64",
		"find_nearby": "bool",
		"sort":        "sort",
		"latitude":    "float64",
		"longitude":   "float64",
	}
)

func ParseSearch(value url.Values) map[string]interface{} {
	mapSearch := make(map[string]interface{})
	// set default
	mapSearch["limit"] = defaultLimit
	mapSearch["page"] = int64(1)

	for key, _ := range value {
		val := value.Get(key)
		switch valType[key] {
		case "bool":
			mapSearch[key], _ = strconv.ParseBool(val)
		case "int64":
			mapSearch[key], _ = strconv.ParseInt(val, 10, 64)
		case "float64":
			mapSearch[key], _ = strconv.ParseFloat(val, 64)
		case "sort":
			mapSearch["sort_cond"] = "ASC"
			if val[0:1] == "-" {
				mapSearch["sort_cond"] = "DESC"
				val = val[1:]
			}
			mapSearch["sort_field"] = val
		default:
			mapSearch[key] = val
		}
	}

	return mapSearch
}
