package user

import "fmt"

type UserPayload struct {
	UserName    string `json:"user_name"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phone_number"`
	Roles       string `json:"roles"`
}

type RefreshTokenPayload struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}

type LoginPayload struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

func (u *LoginPayload) Validate() error {
	if u.UserName == "" {
		return fmt.Errorf("user_name is missing")
	}
	if u.Password == "" {
		return fmt.Errorf("password is missing")
	}

	return nil
}

func (u *UserPayload) Validate() error {
	if u.UserName == "" {
		return fmt.Errorf("user_name is missing")
	}
	if u.Password == "" {
		return fmt.Errorf("password is missing")
	}
	if u.PhoneNumber == "" {
		return fmt.Errorf("phone_number is missing")
	}
	if u.Roles == "" {
		return fmt.Errorf("roles is missing")
	}

	return nil
}
