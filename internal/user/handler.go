package user

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var (
	ioutilReadAll = ioutil.ReadAll
)

type UserHandler struct {
	userService UserService
}

func NewUserHandler(userService UserService) (*UserHandler, error) {
	return &UserHandler{
		userService: userService,
	}, nil
}

func (uh *UserHandler) Register(router *httprouter.Router) {
	router.HandlerFunc("POST", "/user/register", uh.RegisterUser)
	router.HandlerFunc("POST", "/login", uh.Login)
	router.HandlerFunc("POST", "/refresh_token", uh.RefreshToken)
}

func (uh *UserHandler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload UserPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = payload.Validate(); err != nil {
		log.Printf("Error while validate payload : %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := uh.userService.Register(payload)
	if err != nil {
		log.Printf("Error while create user : %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(user)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(res)
}

func (uh *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload LoginPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = payload.Validate(); err != nil {
		log.Printf("Error while validate payload : %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := uh.userService.Login(payload)
	if err != nil {
		log.Printf("Error while create user : %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(user)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}

func (uh *UserHandler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload RefreshTokenPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	token, err := uh.userService.RefreshToken(payload)
	if err != nil {
		log.Printf("Error while refresh token: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(token)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}
