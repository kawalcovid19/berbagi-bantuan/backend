package user

import (
	"fmt"
	"time"

	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	Register(payload UserPayload) (*models.User, error)
	Login(payload LoginPayload) (*models.Token, error)
	RefreshToken(payload RefreshTokenPayload) (*models.Token, error)
}

type userService struct {
	userRepo          internal.UserRepository
	tokenExpiryInHour float64
	jwtSignatureKey   string
	tokenIssuer       string
}

func NewUserService(user internal.UserRepository, issuer, tokenSignature string, tokenExpiry float64) (UserService, error) {
	return &userService{
		userRepo:          user,
		tokenIssuer:       issuer,
		tokenExpiryInHour: tokenExpiry,
		jwtSignatureKey:   tokenSignature,
	}, nil
}

func (u *userService) Register(payload UserPayload) (*models.User, error) {
	user := new(models.User)

	user.ID = uuid.New().String()
	user.UserName = payload.UserName
	hashedPassword, err := hashPassword(payload.Password)
	if err != nil {
		return nil, err
	}

	user.Password = hashedPassword
	user.PhoneNumber = payload.PhoneNumber
	user.CreatedAt = time.Now().UTC()
	user.UpdatedAt = time.Now().UTC()
	user.Roles = payload.Roles

	userRegistered, err := u.userRepo.IsUserRegistered(payload.UserName, payload.PhoneNumber)

	if userRegistered {
		return nil, err
	}

	user, err = u.userRepo.Save(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_REFRESH_TOKEN_KEY = []byte("bersama lawan korona")

func (u *userService) RefreshToken(payload RefreshTokenPayload) (*models.Token, error) {
	jwtSignatureKey := []byte(u.jwtSignatureKey)
	_, err := kit.DecodeToken(payload.RefreshToken, JWT_SIGNING_METHOD, JWT_REFRESH_TOKEN_KEY)
	if err != nil {
		return nil, err
	}

	oldToken, err := kit.DecodeToken(payload.Token, JWT_SIGNING_METHOD, jwtSignatureKey)
	if err != nil {
		return nil, err
	}

	claims, ok := oldToken.Claims.(jwt.MapClaims)
	if !ok || !oldToken.Valid {
		return nil, fmt.Errorf("old token not valid")
	}

	userData := new(models.User)
	userData.Roles = fmt.Sprintf("%v", claims["roles"])
	userData.UserName = fmt.Sprintf("%v", claims["username"])

	signedToken, err := u.createToken(userData)
	if err != nil {
		return nil, err
	}

	refreshToken, err := u.createRefreshToken()
	if err != nil {
		return nil, err
	}

	token := new(models.Token)
	token.Token = signedToken
	token.UserName = userData.UserName
	token.RefreshToken = refreshToken

	return token, nil
}

func (u *userService) Login(payload LoginPayload) (*models.Token, error) {
	userData, err := u.userRepo.GetUserByUsername(payload.UserName)
	if err != nil {
		return nil, err
	}

	correctPassword := comparePasswords(userData.Password, payload.Password)
	if !correctPassword {
		return nil, fmt.Errorf("wrong password")
	}

	signedToken, err := u.createToken(userData)
	if err != nil {
		return nil, err
	}

	refreshToken, err := u.createRefreshToken()
	if err != nil {
		return nil, err
	}

	token := new(models.Token)
	token.Token = signedToken
	token.UserName = userData.UserName
	token.RefreshToken = refreshToken

	return token, nil
}

func hashPassword(password string) (string, error) {
	bytePassword := []byte(password)

	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword[:]), nil
}

func (u *userService) createRefreshToken() (string, error) {
	issuer := u.tokenIssuer
	expire_duration := time.Duration(24) * time.Hour

	claims := models.Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(expire_duration).Unix(),
		},
	}

	tokenClaims := jwt.NewWithClaims(JWT_SIGNING_METHOD, claims)
	signedToken, err := tokenClaims.SignedString(JWT_REFRESH_TOKEN_KEY)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func (u *userService) createToken(userData *models.User) (string, error) {
	issuer := u.tokenIssuer
	expire_duration := time.Duration(u.tokenExpiryInHour) * time.Hour
	jwt_signature_key := []byte(u.jwtSignatureKey)

	claims := models.Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(expire_duration).Unix(),
		},
		Username: userData.UserName,
		Roles:    userData.Roles,
	}

	tokenClaims := jwt.NewWithClaims(JWT_SIGNING_METHOD, claims)
	signedToken, err := tokenClaims.SignedString(jwt_signature_key)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func comparePasswords(hashedPwd string, plainPwd string) bool {
	byteHash := []byte(hashedPwd)
	bytePlainPwd := []byte(plainPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, bytePlainPwd)
	if err != nil {
		log.Printf("error comparing password: %v", err)
		return false
	}

	return true
}
