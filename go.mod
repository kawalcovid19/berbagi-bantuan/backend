module gitlab.com/kawalcovid19/needs-matchmaking/backend

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/subosito/gotenv v1.2.0
	github.com/vrischmann/envconfig v1.2.0
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	google.golang.org/appengine v1.6.6
)
