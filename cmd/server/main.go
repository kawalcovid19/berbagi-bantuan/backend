package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/server"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/storage/mongodb"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal/beneficiary"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal/category"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal/contributor"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal/user"
)

func main() {
	config, err := kit.LoadConfig()
	if err != nil {
		log.Fatalf("error initiate config : %v", err)
	}

	db, err := mongodb.NewClient(config.DB)
	if err != nil {
		log.Fatalf("error initiate database : %v", err)
	}

	beneficiaryRepos := mongodb.NewBeneficiaryRepository(db)
	categoryRepos := mongodb.NewCategoryRepository(db)
	contributorRepos := mongodb.NewContributorRepository(db)
	userRepos := mongodb.NewUserRepository(db)

	benService, err := beneficiary.NewBeneficiaryService(beneficiaryRepos)
	if err != nil {
		log.Fatalf("error initiating beneficiary service : %v", err)
	}

	benHandler, err := beneficiary.NewBeneficiaryHandler(benService)
	if err != nil {
		log.Fatalf("error initiating handler : %v", err)
	}

	catService, err := category.NewCategoryService(categoryRepos)
	if err != nil {
		log.Fatalf("error initiating category service : %v", err)
	}

	userService, err := user.NewUserService(userRepos, config.TokenIssuer, config.JWTSignatureKey, config.TokenExpiryInHour)
	if err != nil {
		log.Fatalf("error initiating user service : %v", err)
	}

	catHandler, err := category.NewCategoryHandler(catService)
	if err != nil {
		log.Fatalf("error initiating handler : %v", err)
	}

	contributorService, err := contributor.NewContributorService(contributorRepos)
	if err != nil {
		log.Fatalf("error initiating service : %v", err)
	}

	contributorHandler, err := contributor.NewContributorHandler(contributorService)
	if err != nil {
		log.Fatalf("error initiating handler : %v", err)
	}

	userHandler, err := user.NewUserHandler(userService)
	if err != nil {
		log.Fatalf("error initiating handler : %v", err)
	}

	startServer(config, benHandler, catHandler, contributorHandler, userHandler)

}

func startServer(conf *kit.Config, handlers ...server.Handler) {
	h := server.Build(handlers...)
	s := &http.Server{
		Addr:         fmt.Sprintf(":%s", conf.Port),
		Handler:      server.Auth(conf.APIKey, conf.JWTSignatureKey, conf.JwtAuthEnable, h),
		ReadTimeout:  300 * time.Second,
		WriteTimeout: 300 * time.Second,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	go func(s *http.Server) {
		log.Printf("matchmaking-backend service is available at %s", s.Addr)
		if serr := s.ListenAndServe(); serr != http.ErrServerClosed {
			log.Fatal(serr)
		}
	}(s)

	<-sigChan
	log.Printf("Shutting down the matchmaking-backend service...")

	err := s.Shutdown(context.Background())
	if err != nil {
		log.Fatalf("Something wrong when stopping server : %v", err)
		return
	}

	log.Printf("matchmaking-backend service gracefully stopped")
}
