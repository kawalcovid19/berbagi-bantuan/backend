## Need Matchmaking Backend Service

### Owner
- #team-needs-matchmaking

### Prerequisites
- [Git](https://www.atlassian.com/git/tutorials/install-git)
- [Go](https://golang.org/doc/install)

### Getting started
- Install the [prerequisites](#prerequisites)

- Clone this repository
    ```sh
    git clone git@gitlab.com:kawalcovid19/needs-matchmaking/backend.git
    ```

- Copy `.env` from `env.sample` and modify the configuration value appropriately

- Build binary
    ```sh
    make all
    ```
- Run executable file
    ```sh
    JWTAUTHENABLE=true TOKENEXPIRYINHOUR=1 JWTSIGNATUREKEY=secret-key TOKENISSUER=needs-matchmaking PROJECT=needs-match-making-api STAGE=dev PORT=8080 APIKEY=aa DB_DATABASE=matchmaking DB_USERNAME=admin DB_PASSWORD=password DB_HOST=localhost:27017 DB_SSL=false bin/server
    ```
- If you don't have mongodb in your system you can run with docker if you've docker
    ```sh
        docker run -d --name mongo -p 27017:27017 \
        -e MONGO_INITDB_ROOT_USERNAME=admin \
        -e MONGO_INITDB_ROOT_PASSWORD=password \
        mongo
    ```

### To Contribute
- Find Open ticket and Issues list
- create branch name with prefix or suffix as ticket name
- run make help: to get information
- create Merge Request and wait at least 1 reviewer

Thank you for contributing