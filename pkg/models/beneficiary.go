package models

import "time"

type Beneficiary struct {
	ID                  string       `json:"id" bson:"_id"`
	Name                string       `json:"name" bson:"name"`
	Category            string       `json:"category" bson:"category"`
	ProviderName        string       `json:"provider_name" bson:"provider_name"`
	Amount              int          `json:"amount" bson:"amount"`
	ReceiverName        string       `json:"receiver_name" bson:"receiver_name"`
	Phone               string       `json:"phone" bson:"phone"`
	IsVerified          bool         `json:"is_verified" bson:"is_verified"`
	IsWhatsAppAvailable bool         `json:"is_whatsapp_available" bson:"is_whatsapp_available"`
	Note                string       `json:"note" bson:"note"`
	Distance            *Distance    `json:"distance,omitempty" bson:"-"`
	IsActive            bool         `json:"is_active" bson:"is_active"`
	ItemDetails         []ItemDetail `json:"item_details" bson:"item_details"`
	Location            Location     `json:"location" bson:"location"`
	CreatedAt           time.Time    `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time    `json:"updated_at" bson:"updated_at"`
	UpdatedBy           string       `json:"updated_by" bson:"updated_by"`
}
