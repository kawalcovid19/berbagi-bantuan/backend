package models

type Location struct {
	Name        string  `json:"name" bson:"name"`
	Country     string  `json:"country" bson:"country"`
	City        string  `json:"city" bson:"city"`
	Address     string  `json:"address" bson:"address"`
	Longitude   float64 `json:"longitude" bson:"longitude"`
	Latitude    float64 `json:"latitude" bson:"latitude"`
	GeoLocation GeoJson `json:"geo_location" bson:"geo_location"`
}

type geoJsonType string

const (
	pointGeoJson = geoJsonType("Point")
)

type GeoJson struct {
	Type        geoJsonType `json:"type" bson:"type"`
	Coordinates []float64   `json:"coordinates" bson:"coordinatCreate`
}

func CreateGeoJsonPoint(longitude, latitude float64) GeoJson {
	return GeoJson{
		Type:        pointGeoJson,
		Coordinates: []float64{longitude, latitude},
	}
}

func (g *GeoJson) Latitude() float64 {
	return g.Coordinates[1]
}

func (g *GeoJson) Longitude() float64 {
	return g.Coordinates[0]
}

type Distance struct {
	Length float64 `json:"length" bson:"-"`
	Unit   string  `json:"unit" bson:"-"`
}
