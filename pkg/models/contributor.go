package models

import "time"

type Contributor struct {
	ID                  string       `json:"id" bson:"_id"`
	Name                string       `json:"name" bson:"name"`
	Category            string       `json:"category" bson:"category"`
	Amount              int          `json:"amount" bson:"amount"`
	ProviderName        string       `json:"provider_name" bson:"provider_name"`
	Phone               string       `json:"phone" bson:"phone"`
	IsPhoneNumberHidden bool         `json:"is_phone_number_hidden" bson:"is_phone_number_hidden"`
	IsWhatsAppAvailable bool         `json:"is_whatsapp_available" bson:"is_whatsapp_available"`
	Note                string       `json:"note" bson:"note"`
	Distance            *Distance    `json:"distance,omitempty" bson:"-"`
	IsActive            bool         `json:"is_active" bson:"is_active"`
	CreatedAt           time.Time    `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time    `json:"updated_at" bson:"updated_at"`
	ItemDetails         []ItemDetail `json:"item_details" bson:"item_details"`
	Location            Location     `json:"location" bson:"location"`
}
