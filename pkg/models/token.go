package models

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	Roles    string `json:"roles"`
}

type Token struct {
	UserName     string `json:"user_name"`
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}
