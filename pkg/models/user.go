package models

import "time"

type User struct {
	ID          string    `json:"id" bson:"_id"`
	UserName    string    `json:"user_name" bson:"user_name"`
	Password    string    `json:"password" bson:"password"`
	PhoneNumber string    `json:"phone_number" bson:"phone_number"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" bson:"updated_at"`
	Roles       string    `json:"roles" bson:"roles"`
}
