package server

import (
	"errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

const (
	apiKeyHeader   = "x-api-key"
	tokenHeader    = "Authorization"
	healthPath     = "/health"
	userNameHeader = "username"
	rolesHeader    = "roles"
)

var ALLOWED_PATHS = map[string]bool{"/refresh_token": true, "/login": true, "/user/register": true}
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY []byte

func Auth(key string, signatureKey string, jwtAuthEnable bool, h http.Handler) http.Handler {
	JWT_SIGNATURE_KEY = []byte(signatureKey)

	if key == "" {
		return h
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if jwtAuthEnable {
			checkAuth(key, h, w, r)
		} else {
			checkApiKey(key, h, w, r)
		}
	})
}

func checkApiKey(key string, h http.Handler, w http.ResponseWriter, r *http.Request) {
	if key == r.Header.Get(apiKeyHeader) || r.URL.Path == healthPath {
		h.ServeHTTP(w, r)
	} else {
		forbidden(w, r)
	}
}

func checkAuth(key string, h http.Handler, w http.ResponseWriter, r *http.Request) {
	if ALLOWED_PATHS[r.URL.Path] && key == r.Header.Get(apiKeyHeader) {
		h.ServeHTTP(w, r)
	} else if r.URL.Path == healthPath {
		h.ServeHTTP(w, r)
	} else {
		authorizationHeader := r.Header.Get(tokenHeader)
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := kit.DecodeToken(tokenString, JWT_SIGNING_METHOD, JWT_SIGNATURE_KEY)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		temp := new(models.Claims)
		temp.Username, ok = claims["username"].(string)
		if !ok {
			err = errors.New("username not found")
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		temp.Roles, ok = claims["roles"].(string)
		if !ok {
			err = errors.New("role not found")
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		r.Header.Set(userNameHeader, temp.Username)
		r.Header.Set(rolesHeader, temp.Roles)
		h.ServeHTTP(w, r)
	}
}
