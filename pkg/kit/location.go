package kit

import "math"

// Reff: https://www.geodatasource.com/developers/go

// Point location
type Point struct {
	Latitude  float64
	Longitude float64
}

// Distance calculate distance beetwen two point, return in kilometer or in meter if the distance less than 1 km
func Distance(source, dest Point) (float64, string) {
	const PI float64 = 3.141592653589793

	radlat1 := float64(PI * source.Latitude / 180)
	radlat2 := float64(PI * dest.Latitude / 180)

	theta := float64(source.Longitude - dest.Longitude)
	radtheta := float64(PI * theta / 180)

	dist := math.Sin(radlat1)*math.Sin(radlat2) + math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)

	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / PI
	dist = dist * 60 * 1.1515

	// distance in kilometer
	dist = dist * 1.609344
	unit := "km"

	if dist < 1 {
		dist = dist * 1000
		unit = "m"
	}

	return dist, unit
}
