package kit

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

func DecodeToken(tokenString string, signingMethod *jwt.SigningMethodHMAC, signatureKey []byte) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Signing method invalid")
		} else if method != signingMethod {
			return nil, fmt.Errorf("Signing method invalid")
		}

		return []byte(signatureKey), nil
	})

	if err != nil {
		return nil, err
	}

	return token, nil
}
