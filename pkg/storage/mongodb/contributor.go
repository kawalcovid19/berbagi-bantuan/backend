package mongodb

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type contributorRepository struct {
	database string
	client   *mongo.Client
}

func NewContributorRepository(dbClient *Client) internal.ContributorRepository {
	return &contributorRepository{
		database: dbClient.Database,
		client:   dbClient.Client,
	}
}

func (r *contributorRepository) collection() *mongo.Collection {
	return r.client.Database(r.database).Collection("contributors")
}

func (r *contributorRepository) Save(contributor *models.Contributor) (*models.Contributor, error) {
	_, err := r.collection().InsertOne(context.Background(), contributor)
	if err != nil {
		return nil, err
	}

	return contributor, nil
}

func (r *contributorRepository) GetByID(id string) (*models.Contributor, error) {
	result := new(models.Contributor)

	err := r.collection().FindOne(context.Background(), bson.D{{"_id", id}}).Decode(result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	return result, nil
}

func (r *contributorRepository) Get(search map[string]interface{}) ([]*models.Contributor, error) {
	results := make([]*models.Contributor, 0)

	formSearch := mongoSearch(search)
	csr, err := r.collection().Find(context.Background(), formSearch.condition(), formSearch.option())
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	defer csr.Close(context.Background())
	err = csr.All(context.Background(), &results)
	if err != nil {
		return nil, err
	}

	return results, nil
}

func (r *contributorRepository) Count(search map[string]interface{}) (int64, error) {
	opts := options.Count().SetMaxTime(2 * time.Second)

	formSearch := mongoSearch(search)
	count, err := r.collection().CountDocuments(context.Background(), formSearch.condition(searchNear), opts)
	if err != nil {
		return 0, err
	}

	return count, nil
}
