package mongodb

import (
	"context"
	"errors"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type userRepository struct {
	database string
	client   *mongo.Client
}

func NewUserRepository(dbClient *Client) internal.UserRepository {
	return &userRepository{
		database: dbClient.Database,
		client:   dbClient.Client,
	}
}

func (r *userRepository) collection() *mongo.Collection {
	return r.client.Database(r.database).Collection("users")
}

func (r *userRepository) Save(user *models.User) (*models.User, error) {
	_, err := r.collection().InsertOne(context.Background(), user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *userRepository) IsUserRegistered(userName string, phoneNumber string) (bool, error) {
	user := new(models.User)
	filter := bson.D{
		{"$or", bson.A{
			bson.D{{"user_name", userName}},
			bson.D{{"phone_number", phoneNumber}},
		}},
	}
	err := r.collection().FindOne(context.Background(), filter).Decode(user)

	if err != nil {
		return false, err
	}

	return true, errors.New("User already registered")
}

func (r *userRepository) GetUserByUsername(userName string) (*models.User, error) {
	user := new(models.User)
	filter := bson.D{{"user_name", userName}}

	err := r.collection().FindOne(context.Background(), filter).Decode(user)

	if err != nil {
		return nil, err
	}

	return user, nil
}
