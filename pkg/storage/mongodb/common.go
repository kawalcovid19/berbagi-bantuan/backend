package mongodb

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	searchRegex = "$regex"
	searchEqual = "$eq"
	searchNear  = "$near"
)

type mongoSearch map[string]interface{}

var (
	valType = map[string]string{
		"name":             searchRegex,
		"receiver_name":    searchRegex,
		"provider_name":    searchRegex,
		"location.name":    searchRegex,
		"location.country": searchRegex,
		"location.city":    searchRegex,
		"location.address": searchRegex,
		"category":         searchEqual,
		"is_active":        searchEqual,
		"find_nearby":      searchNear,
		"latitude":         "-",
		"longitude":        "-",
	}
)

func (b mongoSearch) condition(skips ...string) bson.D {
	es := bson.D{}
	isSkip := map[string]bool{}
	for _, val := range skips {
		isSkip[val] = true
	}

	for key, act := range valType {

		if isSkip[act] {
			continue
		}

		switch act {
		case "-":
		case "$regex":
			if val, ok := b[key].(string); ok && val != "" {
				es = append(es, bson.E{
					Key:   key,
					Value: bson.D{{"$regex", primitive.Regex{Pattern: val, Options: "i"}}},
				},
				)
			}
		case "$eq":
			if val, ok := b[key]; ok {
				es = append(es, primitive.E{Key: key, Value: val})
			}
		case "$near":
			if val, ok := b[key].(bool); ok && val {
				latitude, okLat := b["latitude"].(float64)
				longitude, okLong := b["longitude"].(float64)
				if okLat && okLong {
					es = append(es,
						bson.E{"location.geo_location",
							bson.D{{"$near",
								bson.D{{"$geometry", bson.D{
									{"type", "Point"},
									{"coordinates", []float64{longitude, latitude}}}}}}}})
				}
			}
		}
	}

	return es
}

func (b mongoSearch) option() *options.FindOptions {
	foption := options.Find().SetLimit(b.limit())
	if b.page() > 1 {
		foption.SetSkip((b.page() - 1) * b.limit())
	}

	if field, cond := b.sort(); field != "" {
		foption.SetSort(map[string]interface{}{field: cond})
	}

	return foption
}

func (b mongoSearch) limit() int64 {
	if limit, ok := b["limit"].(int64); ok {
		return limit
	}
	return defaultLimit
}

func (b mongoSearch) page() int64 {
	if val, ok := b["page"].(int64); ok {
		return val
	}
	return 0
}

func (b mongoSearch) sort() (string, int) {
	cond := 1
	field := ""

	if val, ok := b["sort_cond"].(string); ok && val == "DESC" {
		cond = -1
	}

	if val, ok := b["sort_field"].(string); ok {
		field = val
	}

	return field, cond
}
