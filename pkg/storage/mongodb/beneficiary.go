package mongodb

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type beneficiaryRepository struct {
	database string
	client   *mongo.Client
}

func NewBeneficiaryRepository(dbClient *Client) internal.BeneficiaryRepository {
	return &beneficiaryRepository{
		database: dbClient.Database,
		client:   dbClient.Client,
	}
}

func (r *beneficiaryRepository) collection() *mongo.Collection {
	return r.client.Database(r.database).Collection("beneficiaries")
}

func (r *beneficiaryRepository) Save(beneficiary *models.Beneficiary) (*models.Beneficiary, error) {
	_, err := r.collection().InsertOne(context.Background(), beneficiary)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}

func (r *beneficiaryRepository) GetByID(id string) (*models.Beneficiary, error) {
	result := new(models.Beneficiary)

	err := r.collection().FindOne(context.Background(), bson.D{{"_id", id}}).Decode(result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	return result, nil
}

func (r *beneficiaryRepository) Get(search map[string]interface{}) ([]*models.Beneficiary, error) {
	results := make([]*models.Beneficiary, 0)

	formSearch := mongoSearch(search)
	csr, err := r.collection().Find(context.Background(), formSearch.condition(), formSearch.option())
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	defer csr.Close(context.Background())
	err = csr.All(context.Background(), &results)
	if err != nil {
		return nil, err
	}

	return results, nil
}

func (r *beneficiaryRepository) Count(search map[string]interface{}) (int64, error) {
	opts := options.Count().SetMaxTime(2 * time.Second)

	formSearch := mongoSearch(search)
	count, err := r.collection().CountDocuments(context.Background(), formSearch.condition(searchNear), opts)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (r *beneficiaryRepository) Update(id string, beneficiary *models.Beneficiary) (*models.Beneficiary, error) {
	_, err := r.collection().UpdateOne(
		context.Background(),
		bson.D{{"_id", id}},
		bson.D{{"$set", bson.D{
			{"is_verified", beneficiary.IsVerified},
			{"updated_at", beneficiary.UpdatedAt},
			{"updated_by", beneficiary.UpdatedBy},
		}}},
	)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	return beneficiary, nil
}
