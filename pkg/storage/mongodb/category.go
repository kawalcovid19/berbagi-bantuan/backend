package mongodb

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type categoryRepository struct {
	database string
	client   *mongo.Client
}

func NewCategoryRepository(dbClient *Client) internal.CategoryRepository {
	return &categoryRepository{
		database: dbClient.Database,
		client:   dbClient.Client,
	}
}

func (r *categoryRepository) collection() *mongo.Collection {
	return r.client.Database(r.database).Collection("categories")
}

func (r *categoryRepository) GetByID(id string) (*models.Category, error) {
	result := new(models.Category)

	err := r.collection().FindOne(context.Background(), bson.D{{"_id", id}}).Decode(result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	return result, nil
}

func (r *categoryRepository) Get() ([]*models.Category, error) {
	results := make([]*models.Category, 0)
	csr, err := r.collection().Find(context.Background(), bson.D{{}})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("not found")
		}
		return nil, err
	}

	defer csr.Close(context.Background())
	err = csr.All(context.Background(), &results)
	if err != nil {
		return nil, err
	}

	return results, nil
}
